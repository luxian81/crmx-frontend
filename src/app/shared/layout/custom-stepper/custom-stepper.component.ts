// import { Component, OnInit } from '@angular/core';
import { Directionality } from '@angular/cdk/bidi';
import { ChangeDetectorRef, Component, Input, QueryList } from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';

@Component({
  selector: 'app-custom-stepper',
  templateUrl: './custom-stepper.component.html',
  styleUrls: ['./custom-stepper.component.scss'],
  providers: [{ provide: CdkStepper, useExisting: CustomStepperComponent }],
})

export class CustomStepperComponent extends CdkStepper {
  stepList = [
    { id: 0, name: 'Basic Info' },
    { id: 1, name: 'Start a Service' },
    {
      id: 2, name: 'Profile Details', children: [
        { id: 3, name: 'Education' },
        { id: 4, name: 'Resume' },
        { id: 5, name: 'Certifications' },
        { id: 6, name: 'Skills' },
        { id: 7, name: 'Languages' },
        { id: 8, name: 'Headline and Professional Bio' },
        { id: 9, name: 'Portfolio' },
        { id: 10, name: 'Profile Picture' }]
    },
    { id: 11, name: 'Services Questions' },
    { id: 12, name: 'Assessments' }];

  isVisible = false;
  selectedParentIndex = 0;
  selectedChildIndex = 0;

  constructor(dir: Directionality, changeDetectorRef: ChangeDetectorRef) {
    super(dir, changeDetectorRef);
  }

  onClick(index) {
    this.selectedParentIndex = index;
    if ((this.selectedIndex === 2) || (this.selectedIndex === 3)
      || (this.selectedIndex === 4) || (this.selectedIndex === 8) || (this.selectedIndex === 9)) {
      this.isVisible = true;
    } else {
      this.isVisible = false;
    }
    const inde = this.stepList[index].id;
    this.selectedIndex = inde;
    this.isVisible = true;
  }

  onChildClick(i, j) {
    const inde = this.stepList[i].children[j].id;
    this.selectedIndex = inde - 1;
    if ((this.selectedIndex === 2) || (this.selectedIndex === 3)
      || (this.selectedIndex === 4) || (this.selectedIndex === 8) || (this.selectedIndex === 9)) {
      this.isVisible = true;
    } else {
      this.isVisible = false;
    }
    if (this.selectedChildIndex === 0) {
      this.selectedParentIndex = i;
      this.selectedChildIndex = j;
    } else {
      this.selectedParentIndex = i;
      this.selectedChildIndex = j++;
      this.isVisible = true;
    }

  }

  backPage() {
    this.selectedIndex = this.selectedIndex - 1;
    if ((this.selectedIndex === 2) || (this.selectedIndex === 3)
      || (this.selectedIndex === 4) || (this.selectedIndex === 8) || (this.selectedIndex === 9)) {
      this.isVisible = true;
    } else {
      this.isVisible = false;
    }
  }

  nextPage() {
    this.selectedIndex = this.selectedIndex + 1;
    if ((this.selectedIndex === 2) || (this.selectedIndex === 3) ||
      (this.selectedIndex === 4) || (this.selectedIndex === 8) || (this.selectedIndex === 9)) {
      this.isVisible = true;
    } else {
      this.isVisible = false;
    }
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multiple-select',
  templateUrl: './multiple-select.component.html',
  styleUrls: ['./multiple-select.component.scss']
})
export class MultipleSelectComponent implements OnInit {
  experienceList = [
    {id: 1, name: 'Oil and Gas'},
    {id: 2, name: 'E-commerce'},
    {id: 3, name: 'Option 9'},
    {id: 4, name: 'Construction'},
    {id: 5, name:  'Retail'},
    {id: 6, name:  'Option 10'},
    {id: 7, name: 'Tech'},
    {id: 8, name: 'Marketing'},
    {id: 9, name: 'Option 11'},
    {id: 10, name: 'Healthcare'},
    {id: 11, name: 'Human Resources'},
    {id: 12, name: 'Option 12'},
  ];

  constructor() { }

  ngOnInit(): void {
    this.displayDataInConsole();
  }

  displayDataInConsole() {
    console.log(
      '%cDummy Data For Multiple Select',
      'color:red;font-family:system-ui;font-size:1rem;font-weight:bold'
    );
    console.table(this.experienceList); // this console log is for displayed data passed during demo

  }

}

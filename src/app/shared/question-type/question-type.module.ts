import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MultipleChoiceComponent} from './multiple-choice/multiple-choice.component';
import {MultipleSelectComponent} from './multiple-select/multiple-select.component';
import {BooleanComponent} from './boolean/boolean.component';
import {TextEntryComponent} from './text-entry/text-entry.component';



@NgModule({
  declarations: [
    MultipleChoiceComponent,
    MultipleSelectComponent,
    BooleanComponent,
    TextEntryComponent,
  ],
  exports: [
    BooleanComponent,
    MultipleChoiceComponent,
    MultipleSelectComponent,
    TextEntryComponent
  ],
  imports: [
    CommonModule
  ]
})
export class QuestionTypeModule { }

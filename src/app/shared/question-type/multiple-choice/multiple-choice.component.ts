import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-multiple-choice',
  templateUrl: './multiple-choice.component.html',
  styleUrls: ['./multiple-choice.component.scss']
})
export class MultipleChoiceComponent implements OnInit {
  @Input() question;

  constructor() { }

  ngOnInit(): void {
    this.displayDataInConsole();
  }

  displayDataInConsole() {
    console.log(
      '%cDummy Data For Multiple Choice',
      'color:red;font-family:system-ui;font-size:1rem;font-weight:bold'
    );
    console.table(this.question); // this console log is for displayed data passed during demo

  }

}

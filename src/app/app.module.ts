import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/layout/header/header.component';
import { HomeComponent } from './modules/home/home.component';
import { LoginComponent } from './modules/login/login.component';
import { BasicInfoComponent } from './modules/basic-info/basic-info.component'
import { CustomStepperComponent } from './shared/layout/custom-stepper/custom-stepper.component'
import { CdkStepperModule } from '@angular/cdk/stepper';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { GetStartedComponent } from './modules/get-started/get-started.component';
import { SignUpComponent } from './modules/sign-up/sign-up/sign-up.component';
import { LightboxModule } from 'ngx-lightbox';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PasswordPatternDirective } from './core/directives/password-pattern.directive';
import { MatchPasswordDirective } from './core/directives/match-password.directive';
import { PaginationComponent } from './shared/pagination/pagination/pagination.component';
import {QuestionTypeModule} from './shared/question-type/question-type.module';
import { ChoosePlanComponent } from './components/choose-plan/choose-plan.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    BasicInfoComponent,
    CustomStepperComponent,
    GetStartedComponent,
    SignUpComponent,
    PasswordPatternDirective,
    MatchPasswordDirective,
    PaginationComponent,
    ChoosePlanComponent,

  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    CdkStepperModule,
    ReactiveFormsModule,
    FormsModule,
    LightboxModule,
    FormsModule,
    NgbModule,
    LightboxModule,
    QuestionTypeModule
  ],
  providers: [],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

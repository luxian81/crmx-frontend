import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from './modules/home/home.component';
import { LoginComponent } from './modules/login/login.component';
import { SignUpComponent } from './modules/sign-up/sign-up/sign-up.component';
import { BasicInfoComponent } from './modules/basic-info/basic-info.component';
import { GetStartedComponent } from './modules/get-started/get-started.component';
import {ChoosePlanComponent} from './components/choose-plan/choose-plan.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'providers',
    loadChildren:  () => import('./modules/providers/providers.module').then(m => m.ProvidersModule),
  }
  ,
  {
    path: 'jobs',
    loadChildren: () => import('./modules/jobs/jobs.module').then(m => m.JobsModule),
  },
  {
    path: 'sign-up',
    component: SignUpComponent
  },
  {
    path: 'basic-info',
    component: BasicInfoComponent
  },
  {
    path: 'get-started',
    component: GetStartedComponent
  },
  {
    path: 'assessment',
    loadChildren: () => import('./components/assessment/assessment.module').then(m => m.AssessmentModule)
  },
  {
    path: 'plan',
    component: ChoosePlanComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

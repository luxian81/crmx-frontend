import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';

@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss']
})
export class AssessmentComponent implements OnInit {
  assessmentForm: FormGroup;
  question = {id: 1, question: '2 + 2 = ?', options: ['1', '2', '3', '4', '5']};

  constructor(private fb: FormBuilder, private location: Location) {
  }

  ngOnInit(): void {
    this.assessmentForm = this.fb.group({
      firstInput: ['']
    });
  }

  backPage() {
    this.location.back();
  }

}

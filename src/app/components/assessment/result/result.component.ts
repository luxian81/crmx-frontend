import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  isPass = true; // is pass displays another page

  constructor(private location: Location) { }

  ngOnInit(): void {
  }

  backPage() {
  }

  displayAnotherPage() {
    // remove this function later
    this.isPass = !this.isPass;
  }
}

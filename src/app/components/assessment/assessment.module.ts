import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssessmentComponent } from './assessment/assessment.component';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {QuestionTypeModule} from '../../shared/question-type/question-type.module';
import { ResultComponent } from './result/result.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: AssessmentComponent
  },
  {
    path: 'result',
    pathMatch: 'full',
    component: ResultComponent
  },
];



@NgModule({
  declarations: [AssessmentComponent, ResultComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    QuestionTypeModule,
  ]
})
export class AssessmentModule { }

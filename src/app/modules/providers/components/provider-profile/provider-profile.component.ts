
import { Component, OnInit,  ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Map, AuthenticationType } from 'azure-maps-control';

import { Lightbox } from 'ngx-lightbox';
import { LightboxConfig } from 'ngx-lightbox';
import { CanonicalService } from '../../../../core/services/canonical.service'
@Component({
  selector: 'app-provider-profile',
  templateUrl: './provider-profile.component.html',
  styleUrls: ['./provider-profile.component.scss']
})
export class ProviderProfileComponent implements OnInit, AfterViewInit {
  
  constructor(
    private _lightbox: Lightbox, 
    private _lightboxConfig: LightboxConfig, 
    private metaTagService: Meta, 
    private canonicalService: CanonicalService
    ) {
    _lightboxConfig.centerVertically = true;
    
   }

   url = window.location.href;
   title = 'QKLY Provider - Steven J';

   @ViewChild('map', { static: true })
   public mapContainer: ElementRef;

   public ngAfterViewInit(): void {
    const map = new Map(this.mapContainer.nativeElement, {
        center: [-122.33, 47.6],
        zoom: 12,
        view: "Auto",
        style: 'road',
        language: 'en-US',
        authOptions: {
            authType: AuthenticationType.subscriptionKey,
            subscriptionKey: '1NCFljVtkFOjCzsD3ZrOZXaiu7IPoNN94vLv69TjRa8'
        }
    });

    map.setStyle({
      showLogo: false
    });
}


  slides = [
    {src: "assets/providers/portafolio/portafolio1.png", thumb:"assets/providers/portafolio/portafolio1.png", caption:"Lorem ipsum dolor sit amet, consectetur adipiscing elit"},
    {src: "assets/providers/portafolio/portafolio2.png", thumb:"assets/providers/portafolio/portafolio1.png", caption:"Lorem ipsum dolor sit amet, consectetur adipiscing elit"},
    {src: "assets/providers/portafolio/portafolio3.png", thumb:"assets/providers/portafolio/portafolio1.png", caption:"Lorem ipsum dolor sit amet, consectetur adipiscing elit"},
    {src: "assets/providers/portafolio/portafolio2.png", thumb:"assets/providers/portafolio/portafolio1.png", caption:"Lorem ipsum dolor sit amet, consectetur adipiscing elit"} 
  ];
  slideConfig = {
    "slidesToShow": 3,
    "slidesToScroll": 1,
    "infinte":false,
    "speed":300,
    "nextArrow": '<img src="assets/icons/left_arrow.png" style="width:20px;height:30px;left: 20px;" class="slick-prev arr slick-arrow">',
    "prevArrow": '<img src="assets/icons/right_arrow.png" style="width:20px;height:30px;right:15px;z-index:1" class="slick-next arr slick-arrow">',
  };
  toCopy;
  
  ngOnInit(): void {
    this.canonicalService.setCanonicalURL();

    this.metaTagService.addTags([
      { name: 'og:url', content: 'https://qkly-marketplace-ui.azurewebsites.net/provider-profile' },
      { name: 'og:type', content: 'article' },
      { name: 'og:title', content: 'QKLY Provider - Steven J' },
      { name: 'og:description', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' },
      { name: 'og:image', content: 'https://qkly-marketplace-ui.azurewebsites.net/assets/providers/avatar.png' },
      { charset: 'UTF-8' }
    ]);
  }


  slickInit(e) {
    console.log('slick initialized');
  }

  open(index: number): void {
    // open lightbox
    this._lightbox.open(this.slides, index);
  }

  copyText(){
    this.toCopy = document.getElementById("toCopy");
    this.toCopy.select();
    this.toCopy.setSelectionRange(0, 99999); /*For mobile devices*/
    document.execCommand("copy");
    alert("Copied the text: " + this.toCopy.value);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ProviderRoutingModule } from './provider-routing.module';
import { ProviderProfileComponent } from './components/provider-profile/provider-profile.component';
import { ProviderListingComponent } from './components/provider-listing/provider-listing.component';


@NgModule({
  declarations: [ProviderProfileComponent, ProviderListingComponent],
  imports: [
    CommonModule,
    ProviderRoutingModule,
    SlickCarouselModule
  ]
})
export class ProvidersModule {}

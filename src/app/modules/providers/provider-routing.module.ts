import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderListingComponent } from './components/provider-listing/provider-listing.component';
import { ProviderProfileComponent } from './components/provider-profile/provider-profile.component';

const routes: Routes = [
  {
    path: '',
    children: [
       { path:'',  component: ProviderListingComponent },
       { path: 'provider-profile', component: ProviderProfileComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ProviderRoutingModule { }

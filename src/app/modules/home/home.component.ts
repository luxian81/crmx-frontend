import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../core/models/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // name:string;
  // lastName:string;
  // email:string;
  // city:string;
  // state:string;
  // zipCode:string;
  // password:string;
  // cpassword:string;

  userModal = new User();


  constructor(private router: Router) { }

  ngOnInit(): void {}

  processForm() {
    this.router.navigateByUrl(`/sign-up`);
  }


}

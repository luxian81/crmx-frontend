import { Component, OnInit,  ViewChild, ElementRef, AfterViewInit } from '@angular/core';
//import { Component, OnInit } from '@angular/core';
import { Map, AuthenticationType } from 'azure-maps-control';

@Component({
  selector: 'app-jobprofile',
  templateUrl: './jobprofile.component.html',
  styleUrls: ['./jobprofile.component.scss']
})
export class JobprofileComponent implements OnInit {

  constructor() { }
  showFav1 = false;
  showFav2 = false;
  showFav3 = false;
  showFav4 = false;

  @ViewChild('map', { static: true })
  public mapContainer: ElementRef;
  

  public ngAfterViewInit(): void {
    console.log("ngAfter");
    const map = new Map(this.mapContainer.nativeElement, {
        center: [-122.33, 47.6],
        zoom: 12,
        view: "Auto",
        style: 'road',
        language: 'en-US',
        authOptions: {
            authType: AuthenticationType.subscriptionKey,
            subscriptionKey: '1NCFljVtkFOjCzsD3ZrOZXaiu7IPoNN94vLv69TjRa8'
        }
    });
    
    map.setStyle({
      showLogo: false
    });

}

  ngOnInit(): void {
  }

}
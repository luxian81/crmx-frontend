import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-joblist',
  templateUrl: './joblist.component.html',
  styleUrls: ['./joblist.component.scss']
})
export class JoblistComponent implements OnInit {

  constructor(private router: Router) {
    
  }
  showFav1 = false;
  showFav2 = false;
  showFav3 = false;
  showFav4 = false;
  
   navegarHaciaProfile() {
    this.router.navigate(['./jobs/job-profile']);
   }

  ngOnInit(): void {
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobsRoutingModule } from './jobs-routing.module';

import {JobprofileComponent} from './components/jobprofile/jobprofile.component'
import {JoblistComponent} from './components/joblist/joblist.component'

@NgModule({
  declarations: [JobprofileComponent, JoblistComponent],
  imports: [
    CommonModule,
    JobsRoutingModule
  ]
})
export class JobsModule { }

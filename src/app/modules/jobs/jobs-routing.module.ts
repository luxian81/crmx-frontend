import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {JoblistComponent} from './components/joblist/joblist.component'
import {JobprofileComponent} from './components/jobprofile/jobprofile.component'

const routes: Routes = [
  {
    path: '',
    children:[
      {path:'', component:JoblistComponent}, 
      {path:'job-profile' , component:JobprofileComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobsRoutingModule { }

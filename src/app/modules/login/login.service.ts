import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable} from 'rxjs';

@Injectable()
export class LoginService {

  constructor(private http: HttpClient) {
  }

  login(UserName: string, Password: string) {
    return this.http.post('https://localhost:44328/api/auth/login',
    {UserName, Password});
  }
}
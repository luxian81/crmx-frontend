import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss']
})
export class BasicInfoComponent implements OnInit {
  basicInfoForm: FormGroup;
  firstInputFormGroup: FormGroup;
  secondInputFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup:FormGroup;
  fifthFormGroup : FormGroup;
  sixthFormGroup: FormGroup;
  eighthFormGroup: FormGroup;
  ninthFormGroup: FormGroup;
  tenthFormGroup: FormGroup;
  eleventhFormGroup: FormGroup;
  twelfthFormGroup: FormGroup;
  thirteenthFormGroup: FormGroup;
  // fourteenthFormGroup: FormGroup;
  // fifteenthFormGroup: FormGroup;
  // sixteenthFormGroup: FormGroup;
  imageUpload: FormGroup;
  profilePictureUpload: FormGroup;
  portfolioUpload: FormGroup;
  imageFile: any;
  fileName: any;
  closeResult: string;
  acceptedImageTypes = {'application/pdf': true, 'application/doc': true};

  @ViewChild('imageDrop', {static: false}) imageDrop;

  languageList = ['Afrikaans:af', 'Albanian:sq', 'Amharic:am', 'Arabic:ar', 'Armenian:hy', 'Assamese:as',
    'Azerbaijani:az', 'Bangla:bn', 'Basque:eu', 'Belarusian:be', 'Bosnian:bs', 'Bulgarian:bg', 'Burmese:my',
    'Cantonese:zh-yue', 'Catalan:ca', 'Cebuano:ceb', 'Central Kurdish:ckb', 'Chakma:ccp', 'Cherokee:chr',
    'Chinese (Hong Kong):zh-HK', 'Chinese (Simplified, China):zh-Hans', 'Chinese (Simplified):zh-CN',
    'Chinese (Traditional, Taiwan):zh-Hant', 'Chinese (Traditional):zh-TW', 'Corsican:co', 'Croatian:hr',
    'Czech:cs', 'Danish:da', 'Dutch:nl', 'Dzongkha:dz', 'English:en', 'Esperanto:eo', 'Estonian:et', 'Filipino:fil',
    'Filipino:tl', 'Finnish:fi', 'French:fr', 'Fulah:ff', 'Galician:gl', 'Georgian:ka', 'German:de', 'Greek:el',
    'Gujarati:gu', 'Haitian Creole:ht', 'Hawaiian:haw', 'Hebrew:he', 'Hindi:hi', 'Hmong:hmn', 'Hungarian:hu',
    'Icelandic:is', 'Indonesian:id', 'Inuktitut:iu', 'Irish:ga', 'Italian:it', 'Japanese:ja', 'Javanese:jv',
    'Javanese:jw', 'Kannada:kn', 'Kazakh:kk', 'Khmer:km', 'Korean:ko', 'Kurdish:ku', 'Kyrgyz:ky', 'Lao:lo',
    'Latin:la', 'Latvian:lv', 'Lisu:lis', 'Lithuanian:lt', 'Luxembourgish:lb', 'Macedonian:mk', 'Malagasy:mg',
    'Malay:ms', 'Malayalam:ml', 'Maltese:mt', 'Manipuri (Meitei Mayek):mni-Mtei', 'Maori:mi', 'Marathi:mr',
    'Menominee:mez', 'Mongolian:mn', 'Multiple languages:mul', 'Navajo:nv', 'Nepali:ne', 'Norwegian Nynorsk:nn',
    'Norwegian:nb', 'Norwegian:no', 'Nyanja:ny', 'Odia:or', 'Ojibwa:oj', 'Oneida:one', 'Osage:osa', 'Pashto:ps',
    'Persian:fa', 'Polish:pl', 'Portuguese (Brazil):pt-BR', 'Portuguese (Portugal):pt-PT', 'Punjabi:pa', 'Rohingya:rhg',
    'Romanian:ro', 'Romany:rom', 'Russian:ru', 'Samoan:sm', 'Sanskrit:sa', 'Scottish Gaelic:gd', 'Seneca:see',
    'Serbian:sr', 'Shona:sn', 'Sindhi:sd', 'Sinhala:si', 'Slovak:sk', 'Slovenian:sl', 'Somali:so', 'Southern Uzbek:uzs',
    'Spanish:es', 'Sundanese:su', 'Swahili:sw', 'Swedish:sv', 'Tajik:tg', 'Tamil:ta', 'Tatar:tt', 'Telugu:te',
    'Thai:th', 'Tibetan:bo', 'Tigrinya:ti', 'Turkish:tr', 'Ukrainian:uk', 'Urdu:ur', 'Uyghur:ug', 'Uzbek:uz',
    'Vietnamese:vi', 'Welsh:cy', 'Western Frisian:fy', 'Xhosa:xh', 'Yiddish:yi', 'Yoruba:yo', 'Zulu:zu'];

  assessmentDetail = [
    {id: 1, name: 'Bookkeeper Exam', is_attempted: true},
    {id: 2, name: 'Accounting Exam', is_attempted: false},
    {id: 3, name: 'Balaced Sheet Exam', is_attempted: false},
  ];

  question = {id: 1, question: 'How would you rate yourself?', options: ['1', '2', '3', '4', '5']};

  years = [];
  languages: string[];
  type = 1;

  skillList = [
    {id:1, name:'CFO'},
    {id:2, name:'Controller'},
    {id:3, name:'Cost Accounting'},
    {id:4, name:'Mergers and Acquistions'},
  ]

  constructor(private fb: FormBuilder, private modalService: NgbModal) {
  }

  checkfiles(files) {
    if (this.acceptedImageTypes[files[0].type] !== true) {
      this.imageDrop.nativeElement.innerHTML = 'Not an image';
      return;
    } else if (files.length > 1) {
      this.imageDrop.nativeElement.innerHTML = 'Only one image/time';
      return;
    } else {
      this.readfiles(files);
    }
  }

  ngOnInit(): void {
    this.displayDataInConsole();

    this.fetchLanguage(this.languageList);

    let year = new Date().getFullYear();
    this.years.push(year);
    let i: any;
    for (i = 1; i <= 8; i++) {
      this.years.push(year + i);
    }

    this.imageUpload = this.fb.group({
      imageInput: ['', Validators.required],
      imageName: ['', Validators.required],
      imageDescr: ['']
    });
    this.firstInputFormGroup = this.fb.group({
      firstInput: ['']
    });

    this.secondInputFormGroup = this.fb.group({
      secondInput: ['b']
    });
    this.thirdFormGroup = this.fb.group({
      thirdInput: ['c']
    });
    this.fourthFormGroup = this.fb.group({
      thirdInput: ['c']
    });
    this.fifthFormGroup = this.fb.group({
      thirdInput: ['c']
    });
    this.sixthFormGroup = this.fb.group({
      thirdInput: ['c']
    });
    this.eighthFormGroup = this.fb.group({
      eightInput: ['c']
    });
    this.ninthFormGroup = this.fb.group({
      headline: new FormControl('', [Validators.minLength(50)]),
      professional_bio: new FormControl('', [Validators.minLength(500)])
    });
    this.tenthFormGroup = this.fb.group({
      tenthInput: ['c']
    });
    this.eleventhFormGroup = this.fb.group({
      eleventhInput: ['c']
    });
    this.twelfthFormGroup = this.fb.group({
      eleventhInput: ['']
    });
    this.thirteenthFormGroup = this.fb.group({
      tenthInput: ['c']
    });
    // this.fourteenthFormGroup = this.fb.group({
    //   eleventhInput: ['c']
    // });
    // this.fifteenthFormGroup = this.fb.group({
    //   eleventhInput: ['c']
    // });
  }

  openSkillModal(skillModal) {
    this.modalService.open(skillModal, { backdrop: 'static', windowClass: 'myCustomModal' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  imageChange(event) {
    this.imageDrop.innerHTML = '';
    this.checkfiles(event.target.files);
    this.imageFile = event.target.files;
  }

  readfiles(files) {
    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    this.fileName = files[0].name;
  }

  allowDrop(e) {
    e.preventDefault();
  }

  drop(e) {
    e.preventDefault();
    this.imageUpload.controls.imageInput.reset();
    this.imageDrop.innerHTML = '';
    this.checkfiles(e.dataTransfer.files);
  }

  fetchLanguage(list) {
    this.languages = list.map(val => {
      return val = val.split(':')[0];
    });
  }

  displayDataInConsole() {
    console.log(
      '%cDummy Data for language',
      'color:red;font-family:system-ui;font-size:1rem;font-weight:bold'
    );
    console.log(this.languageList); // this console log is for displayed data passed during demo

    console.log(
      '%cDummy Data for assessment',
      'color:red;font-family:system-ui;font-size:1rem;font-weight:bold'
    );
    console.table(this.assessmentDetail); // this console log is for displayed data passed during demo
  }

  changeComponent() {
    this.type === 4 ? this.type = 1 : this.type++;

  }
}

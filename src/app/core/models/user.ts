export class User {
    public name: string;
    public lastName:string;
    public email:string;
    public city:string;
    public state:string;
    public zipCode:string;
    public password:string;
    public cpassword:string;
}
